#!/bin/bash

# Function to run Postgres in a Docker container

Run_postgres() 
{
    # Print installation message
  echo ""
  echo ""
  echo "#####################################################################"
  echo "#####################################################################"
  echo ""
    
    echo "Install Postgres"
    echo ""

    # Check if container ID is provided
    [ ! -z $1 ] && echo "Creating container: postgres${1}" && ID_CONTENEUR=$1 && echo ""

    # Create directories for Postgres data
    echo "1 - Create directories ${DIR}/postgres${ID_CONTENEUR}/"
    mkdir -p $DIR/postgres${ID_CONTENEUR}/

    # Docker Compose configuration for Postgres
    echo "
version: '3.0'

services:
  postgres${ID_CONTENEUR}:
    image: postgres:latest
    container_name: postgres${ID_CONTENEUR}
    environment:
    - POSTGRES_USER=myuser
    - POSTGRES_PASSWORD=password
    - POSTGRES_DB=mydb
    expose:
    - 5432
#   volumes:
#   - postgres_data${ID_CONTENEUR}:/var/lib/postgresql/data/
    networks:
    - generator     

#volumes:
#  postgres_data${ID_CONTENEUR}:
#    driver: local
#    driver_opts:
#      o: bind
#      type: none
#      device: ${DIR}/postgres${ID_CONTENEUR}

networks:
  generator:
    driver: bridge
    ipam:
      config:
        - subnet: 192.168.168.0/24
" >$DIR/docker-compose-postgres${ID_CONTENEUR}.yml

    # Run Postgres container
    echo "2 - Run postgres"
    docker-compose -f $DIR/docker-compose-postgres${ID_CONTENEUR}.yml up -d

    # Display credentials and connection command
    echo "
Credentials:
    user: myuser
    password: password
    db: mydb
    port: 5432

Command: psql -h <ip> -u myuser mydb
"
  echo ""
  echo "#####################################################################"
  echo "#####################################################################"
  echo ""
  echo ""
}

