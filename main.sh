#!/bin/bash


DIR="${HOME}/generator"
USER_SCRIPT=$USER

## Functions ######################################################


source "$(pwd)/Get_docker_ip.sh"
source "$(pwd)/Display_help.sh"
source "$(pwd)/Run_postgres.sh"
source "$(pwd)/Clean_docker.sh"

## Execute ########################################################

# Define option specifications
optspec=":ihvpc-:"

# Parse command-line options using getopts
while getopts "$optspec" optchar; do
    case "${optchar}" in
        -)
            case "${OPTARG}" in
                postgres)
                    # Get the argument for postgres option
                    arg="${!OPTIND}"
                    OPTIND=$(( $OPTIND + 1 ))
                    
                    # Call the postgres function with the argument
                    Run_postgres "$arg"
                    ;;
                clean)
                    # Get the argument for clean option
                    arg="${!OPTIND}"
                    OPTIND=$(( $OPTIND + 1 ))
                    
                    # Call the clean function with the argument
                    Clean_docker "$arg"
                    ;;
                ip)
                    # Call the ip function
                    Get_docker_ip
                    ;;
                help)
                    echo "Error, refer to the help"
                    # Call the help_list function
                    Display_help
                    ;;
                *)
                    echo "Error, refer to the help"
                    # Call the help_list function
                    Display_help
                    ;;
            esac
            ;;
        i)
            # Call the ip function
            Get_docker_ip
            ;;
        p)
            # Call the postgres function
            Run_postgres
            ;;
        h)
            # Call the help_list function
            Display_help
            ;;
        c)
            #Clean up running Dockers
            Clean_docker
            ;;
        *)
            echo "Error, refer to the help"
            # Call the help_list function
            Display_help
            ;;
    esac
done


