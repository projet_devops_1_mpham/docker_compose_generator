#!/bin/bash


Clean_docker()
{
  echo ""
  echo ""
  echo "#####################################################################"
  echo "#####################################################################"
  echo ""
  
  docker-compose -f $DIR/docker-compose-postgres${ID_CONTENEUR}.yml down
  #[ ! -z ${NAME_CONTENEUR} ] && rm -rf $DIR/${NAME_CONTENEUR}
  rm -f $DIR/docker-compose-postgres${ID_CONTENEUR}.yml
  docker volume prune -f
  docker network prune -f
  
  echo ""
  echo "#####################################################################"
  echo "#####################################################################"
  echo ""
  echo ""
}
