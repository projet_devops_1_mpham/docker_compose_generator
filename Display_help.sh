#!/bin/bash

Display_help()
{

  echo ""
  echo ""
  echo "#####################################################################"
  echo "#####################################################################"
  echo ""
  
  echo "Usage:

  ${0##*/} [-h][-p][-i][--clean]

Options:

  -h, --help					Can I help you ?

  -i, --ip					List ip for each container

  -p, --postgres [ID if you want]		Run postgres 

  --clean [container name]			Remove container and datas
  "
  echo ""
  echo "#####################################################################"
  echo "#####################################################################"
  echo ""
  echo ""
}
