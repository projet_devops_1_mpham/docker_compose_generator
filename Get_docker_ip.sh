#!/bin/bash

Get_docker_ip() {
  echo ""
  echo ""
  echo "############################################"
  echo "############################################"
  echo ""
  
  for i in $(docker ps -q); do
    docker inspect -f "{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}} - {{.Name}}" $i;
    echo ""
  done
  
  echo "############################################"
  echo "############################################"
  echo ""
  echo ""
}

